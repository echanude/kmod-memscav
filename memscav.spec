%global             kmod_version 4
%global             latest_kernel 5.14.0-511%{dist}.%{_arch}
%global             latest_kernel_automotive 5.14.0-509.459%{dist}.%{_arch}

%global             srcrev f8a47f22d1ee50dc4f5e7969d0b30b3d493ad3b5

%if "%{dist}" == ".el9iv"
%{!?kernel_version:%{expand:%%global kernel_version %{latest_kernel_automotive}}}
%else
%{!?kernel_version:%{expand:%%global kernel_version %{latest_kernel}}}
%endif

Name:               memscav
%if "%{dist}" == ".el9iv"
Version:            %{lua:print((string.gsub(string.match(rpm.expand("%{kernel_version}"), "(%d+%.%d+%.%d+%-%d+%.%d+%.?[0-9_]*)%.el9iv"),"-","~")))}.%{kmod_version}
%else
Version:            %{lua:print((string.gsub(string.match(rpm.expand("%{kernel_version}"), "(%d+%.%d+%.%d+%-%d+%.?[0-9_]*)"..rpm.expand("%{dist}")),"-","~")))}.%{kmod_version}
%endif
Release:            1%{?dist}
Summary:            memscav Linux kernel module to recover available memory excluded from boot.

License:            GPLv2
URL:                https://gitlab.com/CentOS/automotive/src/kmod-memscav.git
Source:             https://gitlab.com/CentOS/automotive/src/kmod-memscav/-/archive/%{srcrev}/kmod-memscav-%{srcrev}.tar.gz

ExclusiveArch:      aarch64 x86_64
%global kmodtool_generate_buildreqs 1
%if "%{dist}" == ".el9iv"
BuildRequires:      kernel-automotive-devel-uname-r = %{kernel_version}
%else
BuildRequires:      kernel-devel-uname-r = %{kernel_version}
%endif
BuildRequires:      redhat-rpm-config kernel-rpm-macros elfutils-libelf-devel kmod

%kernel_module_package

%description
Out-of-tree Linux kernel module to allow userspace to try and reclaim memory
that may have been excluded from the physical layout (e.g, by the kernel
parameter `mem=X`). Supports kernel using device-tree (`/memory` node) or UEFI
memory map.


%package systemd
Version:            0.3
Summary:            Systemd units to scavenge memory hidden at boot time.
BuildArch:          noarch
BuildRequires:      systemd-rpm-macros
%{?systemd_requires}
Requires:           kmod-memscav

%description systemd
memscav systemd units to scavenge memory hidden to the kernel at different
point in the bootstrap.


%prep
%setup -T -b 0 -q -n kmod-memscav-%{srcrev}


%build
%make_build -C %{kernel_source default} M=$PWD modules


%install
%make_build -C %{kernel_source default} M=$PWD INSTALL_MOD_PATH=%{buildroot} INSTALL_MOD_DIR=extra modules_install
mkdir -p %{buildroot}%{_libexecdir}/memscav
install -DpZm 0755 usr/libexec/memscav/memscav-initrd %{buildroot}%{_libexecdir}/memscav/memscav-initrd
install -DpZm 0644 usr/lib/systemd/system/memscav-initrd.service %{buildroot}%{_unitdir}/memscav-initrd.service
install -DpZm 0755 usr/libexec/memscav/memscav %{buildroot}%{_libexecdir}/memscav/memscav
install -DpZm 0644 usr/lib/systemd/system/memscav.service %{buildroot}%{_unitdir}/memscav.service
install -DpZm 0755 usr/lib/dracut/modules.d/90memscav/module-setup.sh %{buildroot}%{_prefix}/lib/dracut/modules.d/90memscav/module-setup.sh


%files systemd
%{_libexecdir}/memscav/memscav-initrd
%{_unitdir}/memscav-initrd.service
%{_libexecdir}/memscav/memscav
%{_unitdir}/memscav.service
%{_prefix}/lib/dracut/modules.d/90memscav/module-setup.sh


%post systemd
%systemd_post memscav-initrd.service
%systemd_post memscav.service

%preun systemd
%systemd_preun memscav-initrd.service
%systemd_preun memscav.service

%postun systemd
%systemd_postun_with_restart memscav-initrd.service
%systemd_postun_with_restart memscav.service


%changelog
* Thu Sep 26 2024 Eric Chanudet <echanude@redhat.com> 5.14.0~509.459.4-1
- spec: update source handling and kernels versions

* Thu Mar 28 2024 Eric Chanudet <echanude@redhat.com> 5.14.0~433.385.4-1
- spec: update latest_kernel version

* Thu Mar 21 2024 Eric Chanudet <echanude@redhat.com> 5.14.0~428.380.4-1
- spec: fix latest_kernel version handling

* Mon Jan 08 2024 Eric Chanudet <echanude@redhat.com> 0.3-3
- spec: handle automotive headers with rpm
- spec: install in extra

* Tue Dec 19 2023 Eric Chanudet <echanude@redhat.com> 0.3-2
- Move project to automotive-sig

* Mon Oct 30 2023 Eric Chanudet <echanude@redhat.com> 0.3-1
- memscav: add error codes to error messages
- memscav: handle sytems with uefi memmap

* Mon Oct 09 2023 Eric Chanudet <echanude@redhat.com> 0.2-1
- memscav: fix sysfs group leak
- memscav: fix uninitialized nid in scavenge_store
- memscav: simplify ram_map_from_fdt loop condition
- memscav: disable_unload once memory is handled only
- memscav: fix ranges_show loop size signedness
- memscav: Drop redundant casts
- memscav: fix potential leak in ranges handling
- memscav: do not initialize globals in vain
- memscav: typos in README.md

* Thu Oct 05 2023 Eric Chanudet <echanude@redhat.com> 0.1-1
- memscav: add a specfile rpkg template
- memscav: module to reclaim capped out memory
- gitignore: setup repository
